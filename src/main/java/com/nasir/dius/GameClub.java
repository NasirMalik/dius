package com.nasir.dius;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameClub {

    private BowlingGame game;

    private void initGame() {
        this.game = new BowlingMatch();
        System.out.println("\n****************");
        System.out.println("Staring new game. Enter " +
                "\n'New' anytime to start a new game, " +
                "\n'Exit' any time to quit, or " +
                "\n'Score' to see the scorecard");
        System.out.println("****************\n");
    }

    public static void main(String[] args) {
        GameClub club = new GameClub();
        club.initGame();

        Scanner myObj = new Scanner(System.in);  // Create a Scanner object

        String message = "";
        do {
            System.err.print("\nEnter New, Exit, Score, or Pins to roll : ");
            String noOfPins = myObj.nextLine();

            if ("Exit".equalsIgnoreCase(noOfPins)) {
                club.matchStats();
                System.exit(0);
            } else if ("New".equalsIgnoreCase(noOfPins)) {
                club.initGame();
            }
            else if ("Score".equalsIgnoreCase(noOfPins)) {
                club.matchStats();
            }
            else if (onlyDigits(noOfPins)){
                try {
                    club.game.roll(Integer.parseInt(noOfPins));
                } catch (BowlingException e) {
                    message = e.getMessage();
                    System.err.println(" " + message);
                    if (BowlingGame.GAME_OVER.equalsIgnoreCase(message)) {
                        club.initGame();
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
            else {
                System.err.println("Only allowed inputs are -> [Exit, Score, 0-9 Numeric]");
            }
        }
        while (true);
    }

    private void matchStats() {
        System.out.print("ScoreCard: " + this.game.getScoreCard());  // Output user input
        System.out.println("Final Score is: " + this.game.score());  // Output user input
    }

    private static boolean onlyDigits(String str)
    {
        String regex = "[0-9]+";
        Pattern p = Pattern.compile(regex);
        return p.matcher(str).matches();
    }

}
