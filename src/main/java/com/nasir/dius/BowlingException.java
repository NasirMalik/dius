package com.nasir.dius;

public class BowlingException extends Exception {
    public BowlingException(String message) {
        super(message);
    }
}
