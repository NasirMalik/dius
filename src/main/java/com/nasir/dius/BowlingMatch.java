package com.nasir.dius;

import java.util.*;
import java.util.stream.Collectors;

public class BowlingMatch implements BowlingGame {

    private final String DELIMITER = ", ";
    private final int maxFrames = 9;

    private List<List<Integer>> scoreCard;
    private int currentTry;
    private int currentFrame;

    public BowlingMatch() {
        scoreCard = new ArrayList<>(10);
        currentTry = 0;
        currentFrame = 0;
    }

    public String roll(int noOfPins) throws BowlingException {

        System.out.print("Frame :" + (currentFrame+1) + " Try :" + (currentTry+1) + " -> " + noOfPins);
        String message = NORMAL_TRY;

        if ((noOfPins > 10)
//                || (currentTry == 1 && (scoreCard.get(currentFrame).get(0) + noOfPins) > 10)
        ){
            throw new BowlingException(INVALID_TRY);
        }

        if (scoreCard.size() < currentFrame+1) {
            scoreCard.add(new ArrayList<>());
        }
        scoreCard.get(currentFrame).add(currentTry++, noOfPins);

        if (noOfPins == 10) {
            message = STRIKE;
        }
        if (currentTry == 2) {
            currentTry = 0;
            currentFrame++;
            if (scoreCard.get(currentFrame-1).get(0) + scoreCard.get(currentFrame-1).get(1) == 10) {
                message = SPARE;
                // In case of a Spare in last frame
                if (currentFrame-1 == maxFrames) {
                    currentTry = 2;
                    currentFrame = currentFrame-1;
                }
            }
            // In case of a Strike in last frame
            if (currentFrame-1 == maxFrames && scoreCard.get(currentFrame-1).get(0) == 10) {
                currentTry = 2;
                currentFrame = currentFrame-1;
            }
        } else if (currentTry == 3) {
            currentFrame++;
        }

        System.out.println(message + DELIMITER + "Score -> " + this.score() + " ");

        if (currentFrame > maxFrames) {
            throw new BowlingException(GAME_OVER);
        }

        return message;
    }

    public int score() {
        int score = 0;
        StringJoiner scoreString = new StringJoiner("+");
        for (int i = 0; i < scoreCard.size(); i++) {
            StringJoiner frameScoreString = new StringJoiner("+", "(", ")");

            // add all the current frame scores
            frameScoreString.add(scoreCard.get(i).stream().map(Objects::toString).collect(Collectors.joining("+")));

            // if its a strike in first try and not last frame
            if (scoreCard.size() > i+1 && scoreCard.get(i).get(0) == 10) {
                // add the next 2 bowls (2nd try from current frame already added)
                frameScoreString.add(scoreCard.get(i+1).get(0).toString());
            }

            // else if its a strike in second try and not last frame
            else if (scoreCard.size() > i+1 && scoreCard.get(i).get(1) == 10) {
                // add the next 2 bowls from the next frame
                frameScoreString.add(scoreCard.get(i+1).get(0).toString());
                // if the 2nd try from the next frame is bowled
                if (scoreCard.get(i+1).size() > 1){
                    frameScoreString.add(scoreCard.get(i+1).get(1).toString());
                }
            }

            // else if its a spare in the current frame and not last frame
            else if (scoreCard.get(i).stream().collect(Collectors.summingInt(Integer::intValue)) == 10 && scoreCard.size() > i+1) {
                // add the 1st try from the next frame i.e next bowl
                frameScoreString.add(scoreCard.get(i+1).get(0).toString());
            }

            scoreString.add(frameScoreString.toString());
        }
        System.out.println("\nScore is calculated as : " + scoreString);

        score = scoreString.toString().isEmpty() ? 0 : Arrays.stream(scoreString.toString()
                .replaceAll("\\(", "")
                .replaceAll("\\)", "").split("\\+")
            ).map(Integer::new)
             .collect(Collectors.summingInt(Integer::intValue));

        return score;
    }

    public String getScoreCard() {
        String currentScore = scoreCard.stream()
                .map(frame -> {
                    StringJoiner joiner = new StringJoiner(",", "[", "]");
                    return joiner.add(
                            frame.stream().map(Objects::toString).collect(Collectors.joining(":"))
                    ).toString();
                })
                .collect(Collectors.joining(" "));
        return currentScore;
    }

}
