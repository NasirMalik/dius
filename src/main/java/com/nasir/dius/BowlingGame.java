package com.nasir.dius;

public interface BowlingGame {

    static final String NORMAL_TRY = "Good Try";
    static final String INVALID_TRY = "Max 10 Pins allowed in a try, Try Again";
    static final String SPARE = "Good Work! Its a Spare";
    static final String STRIKE = "Congratulations! Its a Strike";
    static final String GAME_OVER = "Game Over";

    String roll(int noOfPins) throws BowlingException;

    int score();

    String getScoreCard();

}
