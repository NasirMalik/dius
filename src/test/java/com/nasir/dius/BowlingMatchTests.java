package com.nasir.dius;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class BowlingMatchTests {

    private BowlingGame bowlingGame;

    @BeforeEach
    void setUp() {
        bowlingGame = new BowlingMatch();
    }

    @Test
    @Disabled
    @DisplayName("should Not Allow Invalid Frame Pins")
    public void shouldNotAllowInvalidFramePins() {
        System.out.println("\n*****************************");
        System.out.println("should Not Allow Invalid Frame Pins");
        System.out.println("*****************************\n");
        assertThrows(BowlingException.class, () -> {
            bowlingGame.roll(5);
            bowlingGame.roll(6);
        });
    }

    @Test
    @DisplayName("should Not Allow Invalid RollingPins")
    void shouldNotAllowInvalidRollingPins() {
        System.out.println("\n*****************************");
        System.out.println("should Not Allow Invalid RollingPins");
        System.out.println("*****************************\n");
        assertThrows(BowlingException.class, () -> {
            bowlingGame.roll(11);
        });
    }

    @Test
    @DisplayName("should Score A Spare")
    void shouldScoreASpare() throws BowlingException {
        System.out.println("\n*****************************");
        System.out.println("should Score A Spare");
        System.out.println("*****************************\n");
        bowlingGame.roll(4);
        String message = bowlingGame.roll(6);
        assertTrue(message.endsWith(BowlingGame.SPARE));
    }

    @Test
    @DisplayName("should Score A Strike")
    void shouldTestScoreAStrike() throws BowlingException {
        System.out.println("\n*****************************");
        System.out.println("should Score A Strike");
        System.out.println("*****************************\n");
        String message = bowlingGame.roll(10);
        assertTrue(message.endsWith(BowlingGame.STRIKE));
    }

    @Test
    @DisplayName("should Score Normal")
    void shouldScoreNormal() throws BowlingException {
        System.out.println("\n*****************************");
        System.out.println("should Score Normal");
        System.out.println("*****************************\n");
        bowlingGame.roll(3);
        bowlingGame.roll(5);
        bowlingGame.roll(0);
        bowlingGame.roll(8);
        bowlingGame.roll(5);
        bowlingGame.roll(4);
        bowlingGame.roll(1);
        assertEquals("[3:5] [0:8] [5:4] [1]", bowlingGame.getScoreCard());
    }

    @Test
    @DisplayName("should Score Spares n Strikes")
    void shouldScoreSparesnStrikes() throws BowlingException {
        System.out.println("\n*****************************");
        System.out.println("should Score Spares n Strikes");
        System.out.println("*****************************\n");
        bowlingGame.roll(3);
        bowlingGame.roll(7);
        bowlingGame.roll(0);
        bowlingGame.roll(8);
        bowlingGame.roll(10);
        bowlingGame.roll(4);
        bowlingGame.roll(1);
        assertEquals("[3:7] [0:8] [10:4] [1]", bowlingGame.getScoreCard());
    }

    @Test
    @DisplayName("should Roll And Validate")
    void shouldRollAndValidate() throws BowlingException {
        System.out.println("\n*****************************");
        System.out.println("should Roll And Validate");
        System.out.println("*****************************\n");
        bowlingGame.roll(3);
        bowlingGame.roll(7);
        bowlingGame.roll(0);
        bowlingGame.roll(8);
        bowlingGame.roll(10);
        bowlingGame.roll(4);
        bowlingGame.roll(1);
        assertEquals("[3:7] [0:8] [10:4] [1]", bowlingGame.getScoreCard());
    }

    @Test
    @DisplayName("should Score After A Spare")
    void shouldScoreAfterASpare() throws BowlingException{
        System.out.println("\n*****************************");
        System.out.println("should Score After A Spare");
        System.out.println("*****************************\n");
        bowlingGame.roll(3);
        bowlingGame.roll(7);
        bowlingGame.roll(0);
        bowlingGame.roll(8);
        bowlingGame.roll(4);
        bowlingGame.roll(1);
        assertEquals(23, bowlingGame.score());
    }

    @Test
    @DisplayName("should Score After A Strike")
    void shouldScoreAfterAStrike() throws BowlingException {
        System.out.println("\n*****************************");
        System.out.println("should Score After A Strike");
        System.out.println("*****************************\n");
        bowlingGame.roll(3);
        bowlingGame.roll(7);
        bowlingGame.roll(0);
        bowlingGame.roll(8);
        bowlingGame.roll(4);
        bowlingGame.roll(1);
        bowlingGame.roll(10);
        bowlingGame.roll(4);
        bowlingGame.roll(5);
        assertEquals(47, bowlingGame.score());
    }

    @Test
    @DisplayName("should Score 300")
    void shouldScore300() {
        System.out.println("\n*****************************");
        System.out.println("should Score 300");
        System.out.println("*****************************\n");
        for (int i = 0; i < 21; i++) {
            try {
                bowlingGame.roll(10);
            } catch (BowlingException e) {
                System.out.println(e.getMessage());
            }
        }
        assertEquals(300, bowlingGame.score());
    }

    @Test
    @DisplayName("should Bowl A Strike in last frame")
    void shouldBowlAStrikeinLastFrame() {
        System.out.println("\n*****************************");
        System.out.println("should Bowl A Strike in last frame");
        System.out.println("*****************************\n");
        try {
            for (int i = 0; i < 18; i++) {
                bowlingGame.roll(10);
            }
            bowlingGame.roll(0);
            bowlingGame.roll(10);
            bowlingGame.roll(0);
        } catch (BowlingException e) {
            System.out.println(e.getMessage());
        }
        assertTrue(bowlingGame.getScoreCard().endsWith("[0:10:0]"));
    }

    @Test
    @DisplayName("should Bowl A Spare in last frame")
    void shouldBowlASpareinLastFrame() {
        System.out.println("\n*****************************");
        System.out.println("should Bowl A Spare in last frame");
        System.out.println("*****************************\n");
        try {
            for (int i = 0; i < 18; i++) {
                bowlingGame.roll(10);
            }
            bowlingGame.roll(6);
            bowlingGame.roll(4);
            bowlingGame.roll(0);
        } catch (BowlingException e) {
            System.out.println(e.getMessage());
        }
        assertTrue(bowlingGame.getScoreCard().endsWith("[6:4:0]"));
    }

    @Test
    @DisplayName("should Bowl Normal in last frame")
    void shouldBowlNormalinLastFrame() {
        System.out.println("\n*****************************");
        System.out.println("should Bowl Normal in last frame");
        System.out.println("*****************************\n");
        try {
            for (int i = 0; i < 18; i++) {
                bowlingGame.roll(10);
            }
            bowlingGame.roll(6);
            bowlingGame.roll(3);
            bowlingGame.roll(3);
        } catch (BowlingException e) {
            System.out.println(e.getMessage());
        }
        assertTrue(bowlingGame.getScoreCard().endsWith("[6:3]"));
    }

    @AfterEach
    void tearDown() {
        System.out.println(bowlingGame.getScoreCard());
    }
}
