# Bowling Club Implementation

This project was built as a maven project in IntelliJ IDE. The implementation is done with Java 1.8.
The project is already built (maven is not required to run) and you may find an executable jar at `<project-home>bowling-1.0-SNAPSHOT.jar`.

# Run

To run the project, you need java 1.8 installed and following command:

java -jar <jar file absolute path>

e.g:

java -jar <project-home>/bowling-1.0-SNAPSHOT.jar

The expected output will be the interactive console game

# Test

To test the project, run the following at the root folder to test:

mvn clean test (if maven is installed)
OR ./mvnw clean test (if maven in not installed)

This will log the test outputs on the terminal and will genarate the target folder with a number of other sub-folders.
=> To see the report of the tests run look into `surefire-reports` folder

# Build

To Build the project, run the following at the root folder to build :

mvn clean package (if maven is installed)
OR ./mvnw clean package (if maven in not installed)

This will generate the portable and executable jar, as is already present at `<project-home>/target/bowling-1.0-SNAPSHOT.jar`
To run the new built jar run the following command:

java -jar <project-home>/target/bowling-1.0-SNAPSHOT.jar
